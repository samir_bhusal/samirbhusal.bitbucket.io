<link
href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
rel="stylesheet"
/>
<!-- Google Fonts -->
<link
href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
rel="stylesheet"
/>
<!-- MDB -->
<link
href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.11.0/mdb.min.css"
rel="stylesheet"
/>

<!--CSS : Font Awesome -->
</head>
<title>@yield('title')</title>
<body>



<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light  " style="background-color: #e3f2fd;">
<!-- Container wrapper -->
<div class="container-fluid navbar-fixed-top">
  <!-- Toggle button -->
  <button
    class="navbar-toggler"
    type="button"
    data-mdb-toggle="collapse"
    data-mdb-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent"
    aria-expanded="false"
    aria-label="Toggle navigation"
  >
    <i class="fas fa-bars"></i>
  </button>

  <!-- Collapsible wrapper -->
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <!-- Navbar brand -->
    <a class="navbar-brand mt-2 mt-lg-0" href="/" >
      <img
        src="https://codenep.com/logo.png"
        height="45"
        alt="kcc Logo"
        loading="lazy"
        
      />
    </a>
    <!-- Left links -->
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/contact">Contact Us</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/about">About us</a>
      </li>
    </ul>
    <!-- Left links -->
  </div>
  <!-- Collapsible wrapper -->

  <!-- Right elements -->


    
    <!-- Avatar -->
    <div class="dropdown">
      <a
        class="dropdown-toggle d-flex align-items-center hidden-arrow"
        href="#"
        id="navbarDropdownMenuAvatar"
        role="button"
        data-mdb-toggle="dropdown"
        aria-expanded="false"
      >
       {{-- https://mdbcdn.b-cdn.net/img/new/avatars --}}
        {{-- <img
       
        
          src="https://mdbcdn.b-cdn.net/img/new/avatars"
          class="rounded-circle"
          height="25"
          alt="Black and White Portrait of a Man"
          loading="lazy"
        /> --}}
        <i class="fas fa-user-alt" style="width:50"></i>
      </a>
      <ul
        class="dropdown-menu dropdown-menu-end"
        aria-labelledby="navbarDropdownMenuAvatar"
      >
        <li>
          <a class="dropdown-item" href="login">Log In</a>
        </li>
        <li>
          <a class="dropdown-item" href="register">Sign Up</a>
        </li>
        {{-- <li>
          <a class="dropdown-item" href="#">Logout</a>
        </li> --}}
      </ul>
    </div>
  </div> 

    <!-- Right elements -->
</div>
<!-- Container wrapper -->
</nav>
<!-- Navbar -->


@yield('content')

@yield('about')

@yield('footer')


{{-- js from mdn --}}
<!-- MDB -->
<script
  type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.11.0/mdb.min.js"
></script>
{{-- js from mdn --}}
</body>
</html>