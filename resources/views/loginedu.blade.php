<div class="row">
    <div class="col-md col-left">
        <div class="d-md-none text-center p-4"><img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/logo-main.png" alt="Digital Nepal Logo" width="176" height="51"></div>
        <h4>DN SMS LOGIN</h4>
        <div class="form-container">
            <form id="formLogin" action="" method="post" novalidate="novalidate"><input type="hidden" class="restrict-clear" name="_csrf" value="c3b3c3cc-d5c8-4173-a461-282e373bf44e">
                
                <div class="alert alert-danger error-text d-none always-display"></div>
                <div class="md-form">
                    <i class="material-icons">admin_panel_settings</i>
                    <input id="username" type="text" class="form-control" name="username" autocomplete="off" required="" autofocus="" aria-required="true">
                    <label for="username" class="">Username</label>
                </div>
                <div class="md-form">
                    <i class="material-icons lock">lock</i>
                    <i class="material-icons visibility noselect" style="display: none;">visibility_off</i>
                    <input id="password" type="password" class="form-control" name="password" required="" aria-required="true">
                    <label for="password">Password</label>
                </div>
                <div>
                    
                    <div class="select-wrapper mdb-select md-form colorful-select dropdown-primary"><span class="caret">▼</span><input type="text" class="select-dropdown form-control valid" readonly="true" required="false" data-activates="select-options-accountType" value="" role="listbox" aria-multiselectable="false" aria-disabled="false" aria-required="true" aria-haspopup="true" aria-expanded="false" aria-invalid="false"><ul id="select-options-accountType" class="dropdown-content select-dropdown w-100" style="display: none; width: 265px; position: absolute; top: 41px; left: 0px; opacity: 1;"><li class="" role="option" aria-selected="false" aria-disabled="false"><span class="filtrable"> Admin    </span></li><li class="" role="option" aria-selected="false" aria-disabled="false"><span class="filtrable"> Teacher    </span></li><li class="active selected" role="option" aria-selected="true" aria-disabled="false"><span class="filtrable"> 
                            Student/Parents
                            </span></li></ul><select name="account_type" class="mdb-select md-form colorful-select dropdown-primary initialized" id="accountType" required="" style="position: absolute; top: 1rem; left: 0px; height: 0px; width: 0px; opacity: 0; padding: 0px; pointer-events: none; display: inline!important;" tabindex="-1" aria-required="true" data-stop-refresh="true">
                        <option selected="">Admin</option>
                        <option value="Admin">Teacher</option>
                        <option value="Student">
                            Student/Parents
                        </option>
                    </select><label class="mdb-main-label active">Role</label></div>
                    
                </div>
                <button type="button" onclick="proceedLogin(this)" class="btn btn-primary btn-login waves-effect waves-light">LOG IN</button>
            </form>
        </div>
        <div class="powered-by-text text-muted">Powered by <span class="company-name">E-Digital Nepal Pvt. Ltd.</span></div>
    </div>
    <div class="col-md col-right">
        <div id="sms-preview-carousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#sms-preview-carousel" data-slide-to="0" class=""></li>
                <li data-target="#sms-preview-carousel" data-slide-to="1" class="active"></li>
                <li data-target="#sms-preview-carousel" data-slide-to="2" class=""></li>
            </ol>
            <div class="carousel-inner text-center">
                <div class="carousel-item">
                    <div class="info-item">
                        <div>
                            <img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/logo-white.png" alt="Digital Nepal White Logo" width="173" height="50">
                            <div class="mt-3">We focus on Digitalizing the educational ecosystem of Nepal</div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item active">
                    <div class="info-item">
                        <div>
                            <img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/graphic2.png" alt="Digital System" width="150" height="117">
                            <div class="mt-3">We manage every school activities digitally at one place</div>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="info-item">
                        <div>
                            <img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/graphic3.png" alt="Easy Access &amp; Fully Digital" width="120" height="154">
                            <div class="mt-3">Easy Access, Fully Digital, Secure &amp; Flexible</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>