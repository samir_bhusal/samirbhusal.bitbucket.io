@extends('departments.layout')
  
@section('content')
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Department</h2>
        </div>
        <div class="pull-right">
          <br>  <a class="btn btn-primary" href="{{ route('department.index') }}"> Back</a> <br>
        </div>
    </div>
</div>
   
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
   
<form action="{{ route('department.store') }}" method="POST">
    @csrf
  
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
               <br> <strong>Department Name:</strong> <br>
                <input type="text" name="dep_name" class="form-control" placeholder="dep_name">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
               <br> <strong>Department Head Id:</strong> <br>
               <input type="number" name="dep_head_id" class="form-control" placeholder="Enter Department Head ID">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <br>  <button type="submit" class="btn btn-primary">ADD</button> <br>
        </div>
    </div>
   
</form>
@endsection