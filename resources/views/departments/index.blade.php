@extends('departments.layout')
 
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Departments</h2>
            </div>
            <div class="pull-right">
              <br>  <a class="btn btn-success" href="{{ route('department.create') }}"> Create New Department</a></br>
            </div>
        </div>
    </div>
   
    @if ($message = Session::get('success'))
       <br> <div class="alert alert-success"></br>
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
      <br>  <tr>
            <th>No.</th>
            <th> Department Name</th>
            <th>Department Head Id</th>
            <th width="280px">Action</th>
        </tr> <br>
        @foreach ($departments as $department)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $department->dep_name }}</td>
            <td>{{ $department->dep_head_id }}</td>
            <td>
                
                <a class="btn btn-info" href="{{ route('department.show',$department->id) }}">Show</a>

                <a class="btn btn-primary" href="{{ route('department.edit',$department->id) }}">Edit</a>
             
                <form action="{{ route('department.destroy',$department->id) }}" method="POST">

                @csrf
                @method('DELETE')
  
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
        </td>
    </tr>
    @endforeach
</table>

{!! $departments->links() !!}
  
@endsection 