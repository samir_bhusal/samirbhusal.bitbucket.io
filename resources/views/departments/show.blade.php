@extends('departments.layout')
  
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2> Show Departments</h2>
            </div>
            <div class="pull-right">
              <br>  <a class="btn btn-primary" href="{{ route('department.index') }}"> Back</a> <br>
            </div>
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
               <br> <strong>Department Name:</strong> <br>
                {{ $departments->dep_name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
              <br>  <strong>Department Head Id:</strong> <br>
                {{ $departments->dep_head_id }}
            </div>
        </div>
    </div>
@endsection