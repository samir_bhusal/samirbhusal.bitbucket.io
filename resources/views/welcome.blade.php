@extends('navbar')
@section('title')
Welcome Page
@endsection

@section('content')


{{-- feature authors css --}}

<style>
    .featured-authors[_ngcontent-gpv-c335] 
    {
    padding: 2rem 4em 2.25rem;
    background-color: #fff;
}

h2[_ngcontent-gpv-c335] {
    padding: 0 0 1.25rem;
    margin: 0;
    font-size: 34px;
    color: #000;
    font-family: "IBM Plex Serif",Arial,sans-serif;
}

.material-box-container[_ngcontent-gpv-c335] {
    display: flex;
    box-sizing: border-box;
    justify-content: space-between;
}

.featured-authors[_ngcontent-gpv-c335] .material-box[_ngcontent-gpv-c335]:not(:last-child) {
    margin-right: 1rem;
}
.featured-authors[_ngcontent-gpv-c335] .material-box[_ngcontent-gpv-c335] {
    min-height: 300px;
    height: auto;
    padding: 1.5rem;
}

.material-box[_ngcontent-gpv-c335]:not(:last-child) {
    margin-right: 1.5rem;
}

.material-box[_ngcontent-gpv-c335] {
    display: flex;
    box-sizing: border-box;
    justify-content: center;
    padding: 1.75rem;
    height: 410px;
    max-width: 450px;
    background-color: #fff;
    border: 1px solid #ddd;
    flex: 1 1 auto;
}

.featured-authors[_ngcontent-gpv-c335] .material-box[_ngcontent-gpv-c335] .content-container[_ngcontent-gpv-c335] {
    max-width: 350px;
    line-height: 28px;
    height: auto;
}


.content-container[_ngcontent-gpv-c335] {
    box-sizing: border-box;
    max-width: 350px;
    height: 100%;
    display: flex;
    flex-direction: column;
}

.author-container[_ngcontent-gpv-c335] {
    display: flex;
}


div {
    display: block;
}

.featured-authors[_ngcontent-gpv-c335] .material-box[_ngcontent-gpv-c335] .content-container[_ngcontent-gpv-c335] {
    max-width: 350px;
    line-height: 28px;
    height: auto;
}


.popular-article[_ngcontent-gpv-c335] {
    padding-top: 1.5rem;
}

.publishing-details[_ngcontent-gpv-c335] {
    font-size: .65rem;
    padding-top: 1rem;
    text-transform: uppercase;
    margin-top: auto;
    color: #069;
}

.featured-authors[_ngcontent-gpv-c335] .material-box[_ngcontent-gpv-c335] {
    min-height: 300px;
    height: auto;
    padding: 1.5rem;
}

.material-box[_ngcontent-gpv-c335] {
    display: flex;
    box-sizing: border-box;
    justify-content: center;
    padding: 1.75rem;
    height: 410px;
    max-width: 450px;
    background-color: #fff;
    border: 1px solid #ddd;
    flex: 1 1 auto;
}

.more-link[_ngcontent-gpv-c335] {
    color: #333;
    margin-top: 1rem;
    text-transform: uppercase;
    font-weight: 700;
    text-decoration: none;
    display: flex;
    align-items: center;
    font-size: 1rem;
    font-size: .75rem;
}

@media only screen and (min-width: 768px){
.follow-author-btn[_ngcontent-gpv-c335] {
    max-width: 200px;
}
}

@media only screen and (min-width: 768px){
.body-resp:not(.ea-only) .text-sm-md {
    font-size: 15px !important;
}
}
</style>

{{-- Featured authors css end --}}

<div class="content vh-100 mt-100 text-center">
    <div class="title m-b-md ">
     
       <h1 class="pt-100">Document Management System</h1> 
       
       <div class="content-wrapper" style="min-height: 1259.2px;">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <h2 class="text-center display-4">Search</h2>
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <form action="simple-results.html">
                            <div class="input-group">
                                <input type="search" class="form-control form-control-lg" placeholder="Type your keywords here">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-lg btn-default">
                                        <i class="fa fa-search"></i>
                                    </button> 
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

{{-- feature author section --}}

<section _ngcontent-gpv-c335="" class="featured-authors">
    <h2 _ngcontent-gpv-c335="" class="text-3xl-md">Featured Authors</h2>
    <div _ngcontent-gpv-c335="" class="material-box-container">
        <div _ngcontent-gpv-c335="" class="material-box">
            <div _ngcontent-gpv-c335="" class="content-container">
                <div _ngcontent-gpv-c335="" class="author-container">
                    <a _ngcontent-gpv-c335="" target="_self" class="stats-Author_Image_HP" href="/author/37269460700">
                        <img _ngcontent-gpv-c335="" src="https://ieeexplore.ieee.org/Xplorehelp/cfg//featured-author-images/Mohammad_Obaidat.png" alt="Picture of samir bhusal">
                    </a>
                    <div _ngcontent-gpv-c335="" class="author-text-container">
                        <div _ngcontent-gpv-c335="" class="author-name text-lg-md">
                            <a _ngcontent-gpv-c335="" target="_self" class="stats-Author_Name_HP" href="/author/37269460700">Samir Bhusal</a>
                        </div>
                        <div _ngcontent-gpv-c335="" class="author-location text-sm-md">(Nepal)</div>
                    </div>
                </div>
                <div _ngcontent-gpv-c335="" class="popular-article">
                    <a _ngcontent-gpv-c335="" target="_self" class="popular-article-title text-base-md stats-Author_Featured_Articlel_HP" href="/document/558812"> Full Stack Developer </a>
                </div>
                <div _ngcontent-gpv-c335="" class="publishing-details">
                    </div><xpl-follow-author _ngcontent-gpv-c335="" sourcepage="HP" class="follow-author-btn" _nghost-gpv-c72="">
                        <button _ngcontent-gpv-c72="" class="layout-btn-white stats-Follow_This_Author_HP text-base-md-lh">Follow This Author</button><!----><!----></xpl-follow-author><a _ngcontent-gpv-c335="" target="_self" class="more-link text-sm-md stats-More_From_Author_Name_HP" href="/author/37269460700">MORE FROM Samir Bhusal<i _ngcontent-gpv-c335="" aria-hidden="true" class="fas fa-caret-right"></i></a>
                    </div>
                </div>
                <div _ngcontent-gpv-c335="" class="material-box">
                    <div _ngcontent-gpv-c335="" class="content-container">
                        <div _ngcontent-gpv-c335="" class="author-container">
                            <a _ngcontent-gpv-c335="" target="_self" class="stats-Author_Image_HP" href="/author/37268947300">
                                <img _ngcontent-gpv-c335="" src="https://ieeexplore.ieee.org/Xplorehelp/cfg//featured-author-images/Muriel_Medard.png" alt="Picture of Author Muriel Médard"></a>
                                <div _ngcontent-gpv-c335="" class="author-text-container">
                                    <div _ngcontent-gpv-c335="" class="author-name text-lg-md">
                                        <a _ngcontent-gpv-c335="" target="_self" class="stats-Author_Name_HP" href="/author/37268947300">Sujit Manadhar</a>
                                    </div>
                                    <div _ngcontent-gpv-c335="" class="author-location text-sm-md">(USA)</div>
                                </div>
                            </div>
                            <div _ngcontent-gpv-c335="" class="popular-article">
                                <a _ngcontent-gpv-c335="" target="_self" class="popular-article-title text-base-md stats-Author_Featured_Articlel_HP" href="/document/8630851">Quality Assurance Engineer</a>
                            </div>
                            <div _ngcontent-gpv-c335="" class="publishing-details"></div>
                            <xpl-follow-author _ngcontent-gpv-c335="" sourcepage="HP" class="follow-author-btn" _nghost-gpv-c72="">
                                <button _ngcontent-gpv-c72="" class="layout-btn-white stats-Follow_This_Author_HP text-base-md-lh">Follow This Author</button><!----><!---->
                            </xpl-follow-author><a _ngcontent-gpv-c335="" target="_self" class="more-link text-sm-md stats-More_From_Author_Name_HP" href="/author/37268947300">MORE FROM Sujit Manandhar<i _ngcontent-gpv-c335="" aria-hidden="true" class="fas fa-caret-right"></i></a>
                        </div>
                    </div>
                    <div _ngcontent-gpv-c335="" class="material-box">
                        <div _ngcontent-gpv-c335="" class="content-container">
                            <div _ngcontent-gpv-c335="" class="author-container">
                                <a _ngcontent-gpv-c335="" target="_self" class="stats-Author_Image_HP" href="/author/37078542600">
                                    <img _ngcontent-gpv-c335="" src="https://ieeexplore.ieee.org/Xplorehelp/cfg//featured-author-images/Sanjeevikumar_Padmanaban.png" alt="Picture of Author Sanjeevikumar Padmanaban">
                                </a>
                                <div _ngcontent-gpv-c335="" class="author-text-container">
                                    <div _ngcontent-gpv-c335="" class="author-name text-lg-md">
                                        <a _ngcontent-gpv-c335="" target="_self" class="stats-Author_Name_HP" href="/author/37078542600">Tritha Raj Neupane</a>
                                    </div>
                                    <div _ngcontent-gpv-c335="" class="author-location text-sm-md">(India)</div>
                                </div>
                            </div>
                            <div _ngcontent-gpv-c335="" class="popular-article">
                                <a _ngcontent-gpv-c335="" target="_self" class="popular-article-title text-base-md stats-Author_Featured_Articlel_HP" href="/document/8902094"> DataBase Engineer </a>
                            </div>
                            <div _ngcontent-gpv-c335="" class="publishing-details"></div>
                            <xpl-follow-author _ngcontent-gpv-c335="" sourcepage="HP" class="follow-author-btn" _nghost-gpv-c72="">
                                <button _ngcontent-gpv-c72="" class="layout-btn-white stats-Follow_This_Author_HP text-base-md-lh">Follow This Author</button><!----><!----></xpl-follow-author>
                                <a _ngcontent-gpv-c335="" target="_self" class="more-link text-sm-md stats-More_From_Author_Name_HP" href="/author/37078542600">MORE FROM Tritha Raj <i _ngcontent-gpv-c335="" aria-hidden="true" class="fas fa-caret-right"></i></a>
                            </div>
                        </div><!----></div>
                    </section>
{{--feature author section  --}}

      </div>
       
        
      </div>
</div>
@endsection

@extends('footer')