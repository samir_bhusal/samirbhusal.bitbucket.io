@extends('navbar')
@section('title')
About Page
@endsection

@section('about')
<section id="about" class="side-image about-section"><!-- About -->

    <div class="image-container col-lg-6 col-md-5 col-sm-3 pull-left">
        <div class="about-bg-image-holder">
        </div>
    </div>
    
<div class="container vh-100">
<div class="row" data-anim-type="bounce-in-up" data-anim-delay="400">

    <div class="col-md-6 col-md-offset-6 col-sm-8 signature col-sm-offset-4 about-pad content clearfix">
        <div class="about-info">
            <h1>About</h1> 
            
            <p class="about-sub-title">We are a team of skilled, dedicated, passionate and relentlessly hard-working people always striving and adapting to gain efficiently to enhance creativity and ensure quality.</p>
            
            <p class="text-left">We are a small team working on the web, the area we love. We started in late 2012 creating web solutions for start up companies. In 2013 we began to develop web templates reaching a higher number of clients. Working with customers from different parts of the world allowed us to evolve to where we are today. We are always up to date with the newest web design trends to deliver the best experience to our customers. Our philosophy is to develop high quality products that fit our customers needs.</p>
                
            <p class="text-left">Our team includes designers, programmers, developers, social media specialists, and project managers who co-ordinate and work together in building services that are best for your business and the clients, that is tailored to your needs.</p>
        </div>
    </div>	
    
</div>		
</div>	

</section>
@endsection

@extends('footer')