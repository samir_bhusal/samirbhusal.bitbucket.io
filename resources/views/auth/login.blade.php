










    
        
    
    


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>School Login - Digital Nepal</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/dist/img/favicon.png" type="image/x-icon">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha512-tDXPcamuZsWWd6OsKFyH6nAqh/MjZ/5Yk88T5o+aMfygqNFPan1pLyPFAndRzmOWHKT+jSDzWpJv8krj6x1LMA=="
          crossorigin="anonymous"
          referrerpolicy="no-referrer"/>
    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/css/mdb.min.css">
    <!-- VAG Rounded Font -->
    <link href="https://fonts.cdnfonts.com/css/vag-rounded-next" rel="stylesheet">
    <!-- Custom Login css -->
    <link rel="stylesheet" href="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/css/login-v2.css">
    <!-- JQuery -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .alert {
            font-size: 12px !important;
        }
    </style>
</head>

<body>
<div class="login-panel">
    <div class="circle-design"></div>
    <div class="row">
        <div class="col-md col-left">
            <div class="d-md-none text-center p-4">
                <img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/logo-main.png" alt="Digital Nepal Logo" width="176" height="51">
            </div>
            <h4>DMS Login</h4>
            
            <div class="form-container">
                <form id="formLogin" action="{{ route('login') }}" method="post">
                    @csrf
                    
                    <div class="alert alert-danger error-text d-none always-display"></div>
                    <div class="md-form">
                        <i class="material-icons">admin_panel_settings</i>
                        <input id="email" type="text" class="form-control" name="email" autocomplete="off"
                               required autofocus>
                        <label for="username">{{ __('Email Address') }}</label>
                    </div>
                    <div class="md-form">
                        <i class="material-icons lock">lock</i>
                        <i class="material-icons visibility noselect" style="display: none;">visibility_off</i>
                        <input id="password" type="password" class="form-control" name="password" required>
                        <label for="password">{{ __('Password') }}</label>
                    </div>
                    <div>
                        
                        <!-- <select name="account_type" class="mdb-select md-form colorful-select dropdown-primary"
                                id="accountType" required>
                            <option selected>Admin</option>
                            <option  value="Admin">Teacher</option>
                            <option  value="Student">
                                Student/Parents
                            </option>
                        </select> -->
                        {{-- <label class="mdb-main-label">Role</label> --}}
                    </div>
                    <button type="submit"  class="btn btn-primary btn-login">{{ __('Login') }}</button>
                </form>
            </div>
            <div class="powered-by-text text-muted">Powered by <span
                    class="company-name">CODENEP Technology Pvt. Ltd.</span></div>
        </div>
        <div class="col-md col-right">
            <div id="sms-preview-carousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#sms-preview-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#sms-preview-carousel" data-slide-to="1"></li>
                    <li data-target="#sms-preview-carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner text-center">
                    <div class="carousel-item active">
                        <div class="info-item">
                            <div>
                                <img src="https://codenep.com/logo.png"
                                     alt="CODENEP Technology White Logo" width="200" height="150">
                                {{-- <div class="mt-3">We focus on Digitalizing the educational ecosystem of Nepal</div> --}}
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="info-item">
                            <div>
                                <img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/graphic2.png" alt="Digital System"
                                     width="150" height="117">
                                <div class="mt-3">
                                    <p>We Create Impact</p> 
                                   <p>By Pushing The Boundaries Of Design And Story Telling</p></div>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="info-item">
                            <div>
                                <img src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/graphic3.png"
                                     alt="Easy Access & Fully Digital" width="120" height="154">
                                <div class="mt-3">Easy Access, Fully Digital, Secure & Flexible</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="shapes">
    <img class="shape1" src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/shape1.png" width="300" height="303">
    <img class="shape2" src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/img/circle-lines.png" width="210" height="210">
    <i class="material-icons shape3">trip_origin</i>
    <i class="material-icons shape4">trip_origin</i>
    <div class="text">CODENEP Technology</div>
</div>

<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/js/mdb-new.js"></script>
<!-- Bootstrap Js -->
<script type="text/javascript" src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/js/bootstrap.min.js"></script>
<!-- Jquery validate -->
<script type="text/javascript" src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/plugins/validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript"
        src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/plugins/validation/dist/additional-methods.min.js"></script>
<!-- Custom Loader JS -->
<script type="text/javascript" src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/dist/js/custom-loader.js"></script>
<!-- Login Common JS -->
<script src="https://d1qltsnz4oe7fo.cloudfront.net/kcc/resources/mdbootstrap/js/login-common.js"></script>

{{-- <script>
    const hasEmailLogin = false;

    function proceedLogin(instance) {
        hideErrorMessage();
        const formInstance = $("#formLogin");
        if (isValidForm(formInstance)) {
            showButtonLoader(instance, "Logging in");
            let url = baseUrl + "/login";
            $.post(url, formInstance.serialize())//+ "&ignore=1"
                .done(function (response) {
                    if (response.success)
                        window.location.replace(baseUrl + response.body.redirectUrl);

                    if (response.message)
                        displayErrorMessage(response.message);
                })
                .fail(function (xhr, status, error) {
                    displayErrorMessage("Sorry but something went wrong. Please refresh the page and try again.");
                })
                .always(function () {
                    hideButtonLoader(instance);
                });
        }
    }
</script>

<script type="text/javascript">
    const parameterName = "_csrf";
    const token = "f3bc5cb1-acb1-4094-a95e-202fd18d468f";

    //append CSRF token on each form tag
    $("form").prepend("<input type='hidden' class='restrict-clear' name='" + parameterName + "' value='" + token + "'>");

    //add CSRF token on each ajax request
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': token
        }
    });
</script> --}}

</body>
</html>