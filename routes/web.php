<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DemoController; 
use App\Http\Controllers\DepartmentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
//  });

Route::get('/', [DemoController::class,'index']);

// Route::get('/contact', function () {
//     return view('contact');
// });
Route::get('/contact',[DemoController::class,'contactus']);

Route::get('/about', function () {
    return view('about');
});

Route::get('/depart', function () {
    return view('departmentdashboard');
});

Route::get('/plain', function () {
    return view('plain');
}); 

Route::get('/login',[DemoController::class,'login']);

// Route::get('/welcome',[DemoController::class,'welcomepage'])




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Auth::routes();


Route::resource('department', DepartmentController::class);