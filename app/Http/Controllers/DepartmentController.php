<?php

namespace App\Http\Controllers;
use App\Models\Departments;
use Illuminate\Http\Request;


class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Departments::latest()->paginate(5);
    
        return view('departments.index',compact('departments'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
     
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }
 /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'dep_name' => 'required',
            'dep_head_id' => 'required',
        ]);
    
        Departments::create($request->all());
     
        return redirect()->route('department.index')
                        ->with('success','Department created successfully.');
    }
     
    /**
     * Display the specified resource.
     *
     * @param  \App\Departments  $departments
     * @return \Illuminate\Http\Response
     */
    public function show( $dep_id)
    {
        $departments=Departments::findorfail($dep_id);
        return view('departments.show',compact('departments'));
    } 
 /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Departments  $departments
     * @return \Illuminate\Http\Response
     */
    public function edit( $dep_id)
    {
        $departments=Departments::findorfail($dep_id);
        return view('departments.edit',compact('departments'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $departments
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $dep_id)
    {
        $request->validate([
            'dep_name' => 'required',
            'dep_head_id' => 'required',
        ]);
        $departments=Departments::findorfail($dep_id);
    
        $departments->update($request->all());
    
        return redirect()->route('department.index')
                        ->with('success','Department updated successfully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy( $dep_id)
    {
        //return $dep_id; 
        $departments=Departments::findorfail($dep_id);
        $departments->delete();
    
        return redirect()->route('department.index')
                        ->with('success','Department deleted successfully');
    }
}