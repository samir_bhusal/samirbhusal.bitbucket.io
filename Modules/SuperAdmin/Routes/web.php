<?php
use Illuminate\Routing\Controller;
use Modules\Http\Controllers\SuperAdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('superadmin')->group(function() {
    Route::get('/superadmin', 'SuperAdminController@index');
});

Route::get('/super', 'SuperAdminController@index');

// Route::get('/super', function () {
//     return view('layouts.master');
// });


